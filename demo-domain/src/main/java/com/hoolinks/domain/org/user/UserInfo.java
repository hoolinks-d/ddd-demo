package com.hoolinks.domain.org.user;

import com.hoolinks.org.dto.clientobject.UserInfoVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description 用户信息Domain
 * @Author zzp
 * @since 2021.07.11
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class UserInfo extends UserInfoVO implements Serializable {

    private static final long serialVersionUID = 1L;

    public boolean isDel() {
        if (this.getStatus() != null && this.getStatus().equals(UserInfoStatusEnum.DISABLE.getId())) {
            return true;
        }

        return false;
    }

    public UserInfo transformationStatus(Integer status) {
        this.setStatus(status);
        this.setUpdateTime(new Date());

        return this;
    }

}
