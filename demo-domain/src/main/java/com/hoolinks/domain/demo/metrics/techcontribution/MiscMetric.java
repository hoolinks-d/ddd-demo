package com.hoolinks.domain.demo.metrics.techcontribution;

import com.hoolinks.domain.demo.metrics.*;
import com.hoolinks.domain.demo.metrics.MainMetric;
import com.hoolinks.domain.demo.metrics.SubMetric;
import com.hoolinks.domain.demo.metrics.SubMetricType;

/**
 * Miscellaneous，其他度量，任何的技术亮点都可以添加
 * @author frankzhang
 */
public class MiscMetric extends SubMetric {

    public MiscMetric(){
        this.subMetricType = SubMetricType.Misc;
    }

    public MiscMetric(MainMetric parent) {
        this.parent = parent;
        parent.addSubMetric(this);
        this.subMetricType = SubMetricType.Misc;
    }

    @Override
    public double getWeight() {
        return metricOwner.getWeight().getUnanimousWeight();
    }
}
