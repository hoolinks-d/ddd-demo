package com.hoolinks.org.gatewayimpl.database;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hoolinks.org.gatewayimpl.database.dataobject.UserInfoPO;

/**
 * <p>
 *  用户信息 Mapper 接口
 * </p>
 *
 * @author zzp
 * @since 2021-07-11
 */
public interface UserInfoMapper extends BaseMapper<UserInfoPO> {

}
