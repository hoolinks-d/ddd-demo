package com.hoolinks.app.demo.command;

import com.alibaba.cola.dto.Response;
import com.hoolinks.demo.convertor.UserProfileConvertor;
import com.hoolinks.domain.demo.user.UserProfile;
import com.hoolinks.demo.dto.UserProfileAddCmd;
import com.hoolinks.domain.demo.gateway.UserProfileGateway;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * UserProfileAddCmdExe
 *
 * @author Frank Zhang
 * @date 2019-02-28 6:25 PM
 */
@Component
public class UserProfileAddCmdExe{

    @Resource
    private UserProfileGateway userProfileGateway;

    public Response execute(UserProfileAddCmd cmd) {
        UserProfile userProfile = UserProfileConvertor.toEntity(cmd.getUserProfileCO());
        userProfileGateway.create(userProfile);
        return Response.buildSuccess();
    }
}
