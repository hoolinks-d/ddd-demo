package com.hoolinks.app.demo.command;

import com.alibaba.cola.dto.Response;
import com.hoolinks.demo.convertor.UserProfileConvertor;
import com.hoolinks.domain.demo.user.UserProfile;
import com.hoolinks.demo.dto.UserProfileUpdateCmd;
import com.hoolinks.domain.demo.gateway.UserProfileGateway;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class UserProfileUpdateCmdExe{

    @Resource
    private UserProfileGateway userProfileGateway;

    public Response execute(UserProfileUpdateCmd cmd) {
        UserProfile userProfile = UserProfileConvertor.toEntity(cmd.getUserProfileCO());
        userProfileGateway.update(userProfile);
        return Response.buildSuccess();
    }
}