package com.hoolinks.app.demo.command;

import com.alibaba.cola.dto.Response;
import com.hoolinks.domain.demo.metrics.techinfluence.ATAMetric;
import com.hoolinks.domain.demo.metrics.techinfluence.ATAMetricItem;
import com.hoolinks.domain.demo.metrics.techinfluence.InfluenceMetric;
import com.hoolinks.domain.demo.user.UserProfile;
import com.hoolinks.demo.dto.ATAMetricAddCmd;
import com.hoolinks.domain.demo.gateway.MetricGateway;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * ATAMetricAddCmdExe
 *
 * @author Frank Zhang
 * @date 2019-03-01 11:42 AM
 */
@Component
public class ATAMetricAddCmdExe{

    @Autowired
    private MetricGateway metricGateway;

    public Response execute(ATAMetricAddCmd cmd) {
        ATAMetricItem ataMetricItem = new ATAMetricItem();
        BeanUtils.copyProperties(cmd.getAtaMetricCO(), ataMetricItem);
        ataMetricItem.setSubMetric(new ATAMetric(new InfluenceMetric(new UserProfile(cmd.getAtaMetricCO().getOwnerId()))));
        metricGateway.save(ataMetricItem);
        return Response.buildSuccess();
    }
}
