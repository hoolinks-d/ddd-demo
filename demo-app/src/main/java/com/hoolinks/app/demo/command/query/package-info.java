/**
 * This package contains QueryExecutors which are used to process Query Request.
 * 
 * @author fulan.zjf
 */
package com.hoolinks.app.demo.command.query;