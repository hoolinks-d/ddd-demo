package com.hoolinks.app.org.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hoolinks.org.dto.clientobject.UserInfoVO;
import com.hoolinks.org.gatewayimpl.database.dataobject.UserInfoPO;

import java.util.List;

/**
 * <p>
 *  用户信息 服务类
 * </p>
 *
 * @author zzp
 * @since 2021-07-11
 */
public interface IUserInfoService extends IService<UserInfoPO> {

    void save(UserInfoVO userInfoVO);

    void delete(List<Integer> userIds);

    void batchUpdateStatus(List<Integer> userIds, Integer status);
}
